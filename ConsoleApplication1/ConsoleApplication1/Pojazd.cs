﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace JiPP
{

    abstract class Pojazd
    {


        public delegate void Zadanie();

        public void DodajMetode(Zadanie metoda)
        {
            metoda();
        }

        private string ID;
        public string id
        {
            get { return ID; }
            set { ID = value; }
        }
        private string MarkaPojazdu;
        public string marka_pojazdu
        {
            get { return MarkaPojazdu; }
            set { MarkaPojazdu = value; }
        }

        private string ModelPojazdu;
        public string model_pojazdu
        {
            get { return ModelPojazdu; }
            set { ModelPojazdu = value; }
        }

        private string IleKoniMaPojazd;
        public string ile_koni
        {
            get { return IleKoniMaPojazd; }
            set { IleKoniMaPojazd = value; }
        }

        private string PojemnoscSilnika;
        public string pojemnosc_silnika
        {
            get { return PojemnoscSilnika; }
            set { PojemnoscSilnika = value; }
        }

        private string IloscDrzwi;
        public string ilosc_drzwi
        {
            get { return IloscDrzwi; }
            set { IloscDrzwi = value; }
        }

        private string RokProdukcji;
        public string rok_produkcji
        {
            get { return RokProdukcji; }
            set { RokProdukcji = value; }
        }

        private string CenaWynajmu;
        public string cena_wynajmu
        {
            get { return CenaWynajmu; }
            set { CenaWynajmu = value; }
        }

        private string RodzajSkrzyniBiegow;
        public string skrzynia_biegow
        {
            get { return RodzajSkrzyniBiegow; }
            set { RodzajSkrzyniBiegow = value; }
        }

        private string Klimatyzacja;
        public string klimatyzacja
        {
            get { return Klimatyzacja; }
            set { Klimatyzacja = value; }
        }

        private string Rodzaj_paliwa;
        public string rodzaj_paliwa
        {
            get { return Rodzaj_paliwa; }
            set { Rodzaj_paliwa = value; }
        }
        private string Dostepnosc_pojazdu;
        public string dostepnosc
        {
            get { return Dostepnosc_pojazdu; }
            set { Dostepnosc_pojazdu = value; }
        }

        public Pojazd() { }
        public Pojazd(string Id, string dostepny, string marka, string model, string konie, string pojemnosc, string drzwi, string rocznik, string cena, string skrzynia, string klima, string paliwo)
        {
            id = Id;
            dostepnosc = dostepny;
            marka_pojazdu = marka;
            model_pojazdu = model;
            ile_koni = konie;
            pojemnosc_silnika = pojemnosc;
            ilosc_drzwi = drzwi;
            rok_produkcji = rocznik;
            cena_wynajmu = cena;
            skrzynia_biegow = skrzynia;
            klimatyzacja = klima;
            rodzaj_paliwa = paliwo;
        }

        public abstract void pobierz_dane_z_bazy(string zrodlo_pliku);

        public virtual void wyswietl_pojazd()
        {
            Console.WriteLine("Id pojazdu: " + id);
            Console.WriteLine("Dosteposc pojazdu: " + dostepnosc);
            Console.WriteLine("Marka pojazdu: " + marka_pojazdu);
            Console.WriteLine("Model pojazdu: " + model_pojazdu);
            Console.WriteLine("Ilosc koni mechanicznych: " + ile_koni + "KM");
            Console.WriteLine("Pojemnosc silnika: " + pojemnosc_silnika);
            Console.WriteLine("Liczba drzwi: " + ilosc_drzwi);
            Console.WriteLine("Rok produkcji: " + rok_produkcji + "r");
            Console.WriteLine("Cena wynajmu: " + cena_wynajmu + "zl/24h");
            Console.WriteLine("Rodzaj skrzyni biegow: " + skrzynia_biegow);
            Console.WriteLine("Klimatyzacja: " + klimatyzacja);
            Console.WriteLine("Rodzaj paliwa: " + rodzaj_paliwa);
        }

        public void podaj_oferte(Pojazd[] tab, int n)
        {
            for (int i = 0; i < n; i++)
                tab[i].wyswietl_pojazd();
        }


        public abstract void szukanie();

        public abstract void podaje();

        public abstract void zamowienie();


        //  public virtual void zloz_zamowienie() { }

    }
}
