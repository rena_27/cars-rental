﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace JiPP
{

    class AutoOsobowe : Pojazd
    {
        int N;
        AutoOsobowe[] tmp = new AutoOsobowe[100];
        string Kolor;
        public string kolor
        {
            get { return Kolor; }
            set { Kolor = value; }
        }
        string Pojemnosc_bagaznika;
        public string poj_bagaznika
        {
            get { return Pojemnosc_bagaznika; }
            set { Pojemnosc_bagaznika = value; }
        }
        string Nadwozie;
        public string nadwozie
        {
            get { return Nadwozie; }
            set { Nadwozie = value; }
        }

        public AutoOsobowe() { }
        public AutoOsobowe(string Id, string dostepnosc, string marka, string model, string konie, string pojemnosc, string drzwi, string rocznik, string cena, string skrzynia, string klima, string paliwo, string koloruta, string pojemnoscBagaznika, string nadwozieauta)
            : base(Id, dostepnosc, marka, model, konie, pojemnosc, drzwi, rocznik, cena, skrzynia, klima, paliwo)
        {
            kolor = koloruta;
            poj_bagaznika = pojemnoscBagaznika;
            nadwozie = nadwozieauta;

        }


        override public void pobierz_dane_z_bazy(string zrodlo_pliku)//dodatkowa zmienna ktora przenosi wskaznik ktora funkcja ma byc wywolana z tablica tmp
        {
            try
            {
                StreamReader czytam = new StreamReader(zrodlo_pliku);
                int n = 0;
                do
                {
                    id = czytam.ReadLine();
                    dostepnosc = czytam.ReadLine();
                    marka_pojazdu = czytam.ReadLine();
                    model_pojazdu = czytam.ReadLine();
                    ile_koni = czytam.ReadLine();
                    pojemnosc_silnika = czytam.ReadLine();
                    ilosc_drzwi = czytam.ReadLine();
                    rok_produkcji = czytam.ReadLine();
                    cena_wynajmu = czytam.ReadLine();
                    skrzynia_biegow = czytam.ReadLine();
                    klimatyzacja = czytam.ReadLine();
                    rodzaj_paliwa = czytam.ReadLine();
                    kolor = czytam.ReadLine();
                    poj_bagaznika = czytam.ReadLine();
                    nadwozie = czytam.ReadLine();

                    tmp[n] = new AutoOsobowe(id, dostepnosc, marka_pojazdu, model_pojazdu, ile_koni, pojemnosc_silnika, ilosc_drzwi, rok_produkcji, cena_wynajmu, skrzynia_biegow, klimatyzacja, rodzaj_paliwa, kolor, poj_bagaznika, nadwozie);
                    n++;

                } while (!czytam.EndOfStream);
                N = n;
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Plik 'auto.txt' nie istnieje!", e);
                Console.ReadKey();
            }
        }

        override public void podaje()
        {
            podaj_oferte(tmp, N);
        }

        override public void wyswietl_pojazd()
        {
            base.wyswietl_pojazd();
            Console.WriteLine("kolor auta: " + kolor);
            Console.WriteLine("pojemnosc bagaznika: " + poj_bagaznika + "l");
            Console.WriteLine("nadwozie: " + nadwozie);
            Console.WriteLine("\n..................\n");
        }


        override public void szukanie()
        {
            Console.WriteLine(" wybierz cyfre wg ktorego ktorej cechy chcesz wyszukac:\n1.marka\n2.model\n3.pojemnosc silnika\n4.rok produkcji\n5.cena wynajmu");
            try
            {
                int a = int.Parse(Console.ReadLine());
                int wyniki = 0;
                Console.WriteLine("podaj szukana fraze: ");
                string cechaszukana = Console.ReadLine();
                Console.WriteLine("wyszukane obiekty: \n");

                switch (a)
                {
                    case 1: for (int i = 0; i < N; i++)
                            if (cechaszukana == tmp[i].marka_pojazdu)
                            {
                                tmp[i].wyswietl_pojazd();
                                wyniki++;
                            }
                        if (wyniki == 0) Console.WriteLine("nie znaleziono obiektu z podana fraza"); break;

                    case 2: for (int i = 0; i < N; i++)
                            if (cechaszukana == tmp[i].model_pojazdu)
                            {
                                tmp[i].wyswietl_pojazd();
                                wyniki++;
                            }
                        if (wyniki == 0) Console.WriteLine("nie znaleziono obiektu z podana fraza"); break;

                    case 3: for (int i = 0; i < N; i++)
                            if (cechaszukana == tmp[i].pojemnosc_silnika)
                            {
                                tmp[i].wyswietl_pojazd();
                                wyniki++;
                            }
                        if (wyniki == 0) Console.WriteLine("nie znaleziono obiektu z podana fraza"); break;

                    case 4: for (int i = 0; i < N; i++)
                            if (cechaszukana == tmp[i].rok_produkcji)
                            {
                                tmp[i].wyswietl_pojazd();
                                wyniki++;
                            }
                        if (wyniki == 0) Console.WriteLine("nie znaleziono obiektu z podana fraza"); break;

                    case 5: for (int i = 0; i < N; i++)
                            if (cechaszukana == tmp[i].cena_wynajmu)
                            {
                                tmp[i].wyswietl_pojazd();
                                wyniki++;
                            }
                        if (wyniki == 0) Console.WriteLine("nie znaleziono obiektu z podana fraza"); break;

                    default:
                        {
                            Console.WriteLine("wpisano nieprawidlowa liczbe, sprobuj ponownie");
                            szukanie(); break;
                        }
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine("Niepoprawy format wprowadznych znakow. Stosuj sie wg zalecen", e);
            }
        }
        public override void zamowienie()
        {
            podaj_oferte(tmp, N);
            try
            {
                int pojazd_w_tablicy = 0;
                int cena;
                double cena_do_zaplaty;

                Console.WriteLine("z podanej oferty podaj Id pojazdu ktory Cie interesuje");
                string wybrano = Console.ReadLine();
                for (int i = 0; i < N; i++)
                {
                    if (wybrano == tmp[i].id)
                    {
                        //wywolanie metody ktora zmienia status pojazdu na niedostepny
                        Console.WriteLine("wybrano: ");
                        tmp[i].wyswietl_pojazd();
                        pojazd_w_tablicy = i;
                    }
                }
                Console.WriteLine("na ile dni chcesz wypozyczyc pojazd?(Podaj cyfre)\n(uwaga przy wypozyczeiu pojazdu powyzej 5 dni, znizka 25%!");
                int dni = int.Parse(Console.ReadLine());
                cena = int.Parse(tmp[pojazd_w_tablicy].cena_wynajmu);
                if (dni > 5)
                    cena_do_zaplaty = cena * dni * 0.75;
                else
                    cena_do_zaplaty = cena * dni;
                Console.WriteLine("do zaplaty " + cena_do_zaplaty + "zl");
            }

            catch (FormatException e)
            {
                Console.WriteLine("Niepoprawy format wprowadznych znakow. Stosuj sie wg zalecen", e);
            }
        }
    }
}





