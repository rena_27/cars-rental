﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace JiPP
{

    class Program
    {

        public delegate void Uruchom(string zrodlo_pliku);
        static void Main(string[] args)
        {
            Pojazd auto = new AutoOsobowe();
            Uruchom dzialam = new Uruchom(auto.pobierz_dane_z_bazy);
            dzialam(@"C:\Users\Renata\Source\Repos\cars-rental\ConsoleApplication1\ConsoleApplication1\auta.txt");

            Pojazd van = new Ciezarowy();
            Uruchom dzialam2 = new Uruchom(van.pobierz_dane_z_bazy);
            dzialam2(@"C:\Users\Renata\Source\Repos\cars-rental\ConsoleApplication1\ConsoleApplication1\ciezarowe.txt");
            int a = 10;
            do
            {
                Console.WriteLine("\nco robimy?\n Wpisz:\n1-by zobaczyc oferte.\n2-by wyszukac.\n3-by zamowic \n0-zakonczyc ");
                try
                {
                    a = int.Parse(Console.ReadLine());
                    switch (a)
                    {
                        case 1:
                            Console.WriteLine("podaj typ pojazdu:\n1-auto\n2-ciezarowy ");
                            int wybor = int.Parse(Console.ReadLine());

                            switch (wybor)
                            {
                                case 1: auto.DodajMetode(auto.podaje); break;
                                case 2: van.DodajMetode(van.podaje); break;
                                default: Console.WriteLine("Invalid selection. Please select  1, or 2."); break;
                            }
                            break;

                        case 2:
                            Console.WriteLine("podaj typ pojazdu:\n1-auto\n2-ciezarowy ");
                            int wybor2 = int.Parse(Console.ReadLine());

                            switch (wybor2)
                            {
                                case 1: auto.DodajMetode(auto.szukanie); break;
                                case 2: van.DodajMetode(van.szukanie); break;
                                default: Console.WriteLine("Invalid selection. Please select  1, or 2."); break;
                            }
                            break;

                        case 3: Console.WriteLine("podaj typ pojazdu:\n1-auto\n2-ciezarowy ");
                            int wybor3 = int.Parse(Console.ReadLine());
                            switch (wybor3)
                            {
                                case 1: auto.DodajMetode(auto.zamowienie); break;
                                case 2: van.DodajMetode(van.zamowienie); break;
                                default: Console.WriteLine("Invalid selection. Please select  1, or 2."); break;
                            }
                            break;

                        case 0: Console.WriteLine("..........koniec programu........"); break;
                        default: Console.WriteLine("Invalid selection. Please select 0, 1,2,3."); break;
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Niepoprawy format wprowadznych znakow. Stosuj sie wg zalecen", e);
                }
            } while (a != 0);
            Console.ReadKey();
        }
    }
}